
# Open IA Integration

This project demonstrates how to create a resilient integration with Open IA API.


## References

 - [Open IA Api Reference](https://platform.openai.com/docs/api-reference)
 - [Open IA Api Documentation](https://platform.openai.com/docs/introduction)


## Autor

- [@riquecosta](https://gitlab.com/riquecosta)


## Licenses

[![MIT License](https://img.shields.io/badge/License-MIT-green.svg)](https://choosealicense.com/licenses/mit/)
[![GPLv3 License](https://img.shields.io/badge/License-GPL%20v3-yellow.svg)](https://opensource.org/licenses/)
[![AGPL License](https://img.shields.io/badge/license-AGPL-blue.svg)](http://www.gnu.org/licenses/agpl-3.0)


## Documentation

This project demonstrates how to create API integration with some resilient httpclient.
The presented structure demonstrates how we can create a Core for the project that can be used in API and even console projects.

The core project is responsible for all integration logic. Other projects only use core to call their methods.

You can use Core project as you need.
