using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using OpenIAIntegration.Core.Infra.OpenIA.Models;
using OpenIAIntegration.Core.Services;

namespace OpenIAIntegration.Api.Controllers
{
    [ApiController]
    [Route("openia")]
    public class OpenIAController : ControllerBase
    {
        private readonly OpenIAService _openIAService;
        public OpenIAController(OpenIAService openIAService)
        {
            _openIAService = openIAService;
        }

        /// <summary>
        /// Generates speech from text.
        /// </summary>
        /// <remarks>
        /// return a [format] audio file
        /// </remarks>
        /// <response code="200">Return a file</response>
        [HttpGet("speech")]
        public async Task<IActionResult> Speech([FromQuery] TextToSpeechRequest request)
        {
            var file = await _openIAService.SpeechText(request);
            return File(file, "audio/mpeg", true);
        }

        /// <summary>
        /// Generates some response from chatgpt-3.5-turbo
        /// </summary>
        /// <remarks>
        /// return a completin text
        /// </remarks>
        /// <response code="200">Return a file</response>
        [HttpGet("completion")]
        public async Task<IActionResult> Completion([FromQuery] string text)
        {
            return Ok(await _openIAService.Completion(text));
        }
    }
}