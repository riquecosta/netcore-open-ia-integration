using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OpenIAIntegration.Api.Extensions
{
    public static class ApiApplicationConfiguration
    {
        public static WebApplication ConfigureApp(this WebApplication app)
        {
            // Configure the HTTP request pipeline.
            if (app.Environment.IsDevelopment())
            {
                app.UseSwagger();
                app.UseSwaggerUI();
            }

            app.UseRouting();
            app.UseEndpoints(endpoints =>
                    {
                        _ = endpoints.MapControllers();
                    });
            app.UseStaticFiles();
            app.UseHttpsRedirection();

            app.Run();
            
            return app;
        }
    }
}