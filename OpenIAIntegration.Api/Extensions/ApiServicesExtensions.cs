using System.Reflection;
using System.Text.Json.Serialization;
using OpenIAIntegration.Core;

namespace OpenIAIntegration.Api.Extensions
{
    public static class ApiServicesExtensions
    {
        public static IServiceCollection AddApiCoreModule(this IServiceCollection services)
        {
            services.AddEndpointsApiExplorer();
            services.AddControllers().AddJsonOptions(opt => {
                opt.JsonSerializerOptions.Converters.Add(new JsonStringEnumConverter());
            });
            
            services.AddSwaggerGen(conf => {
                conf.SwaggerDoc("v1", new Microsoft.OpenApi.Models.OpenApiInfo() {
                    Contact = new Microsoft.OpenApi.Models.OpenApiContact() {
                        Email = "riquecosta@gmail.com",
                        Name = "Rique Costa",
                        Url = new Uri("https://riccon.net")
                    },
                    Description = "Open IA Integration Tests",
                    Title = "Open IA Integration",
                    Version = "v1",
                    TermsOfService = new Uri("https://riccon.net"),
                });
                var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
                var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
                conf.IncludeXmlComments(xmlPath);  
            });

            //register core module.
            services.AddOpenIAIntegrationCoreModule();
            return services;
        }
    }
}