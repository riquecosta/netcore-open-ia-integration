using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using OpenIAIntegration.Core.Infra.OpenIA;
using OpenIAIntegration.Core.Infra.OpenIA.Models;

namespace OpenIAIntegration.Core.Services
{
    public class OpenIAService
    {
        private readonly OpenIAClient _client;
        private readonly ILogger _logger;

        public OpenIAService(OpenIAClient client, ILogger<OpenIAService> logger)
        {
            _client = client;
            _logger = logger;
        }

        public async Task<byte[]> SpeechText(TextToSpeechRequest request)
        {
            try
            {
                return await _client.ExecuteAsync<byte[]>("https://api.openai.com/v1/audio/speech", request);
            }
            catch(Exception ex)
            {
                _logger.LogError(ex, $"An error was thrown: {ex.Message}");
                throw;
            }
        }

        public async Task<CompletionResponse> Completion(string text)
        {
            try
            {
                var request =  new CompletionRequest() {
                    messages = new List<MessagesRequest>(),
                    model = "gpt-3.5-turbo"
                };
                request.messages.Add(new MessagesRequest() {
                    content = text,
                    role = "system"
                });

                return await _client.ExecuteAsync<CompletionResponse>("https://api.openai.com/v1/chat/completions", request);
            }
            catch(Exception ex)
            {
                _logger.LogError(ex, $"An error was thrown: {ex.Message}");
                throw;
            }
        }
    }
}