using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OpenIAIntegration.Core.Infra.OpenIA.Models
{
    public class CompletionRequest
    {
        public List<MessagesRequest> messages { get; set; }
        public string model { get; set; } = "gpt-3.5-turbo";
    }

    public class MessagesRequest 
    {
        public string role { get; set; } = "system";
        public string content { get; set; } = "";
    }
}