using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace OpenIAIntegration.Core.Infra.OpenIA.Models
{
    public class CompletionResponse
    {
        public string id { get; set; }
        
        [JsonPropertyName("object")]
        public string objecto { get; set; }

        public double created { get; set; }
        public string model { get; set; }
        public List<ChoicesResponse> choices { get; set; }
        public string finish_reason { get; set; }

        //some other props here.
    }

    public class ChoicesResponse
    {
        public int index { get; set; }
        public MessageResponse message { get; set; }
    }

    public class MessageResponse
    {
        public string role { get; set; }
        public string content { get; set; }
    }
}