using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace OpenIAIntegration.Core.Infra.OpenIA.Models
{
    public class TextToSpeechRequest
    {
        public string model { get; set; } = "tts-1";
        
        [Required]
        [JsonConverter(typeof(StringEnumConverter))]
        public Voices voice { get; set; } = Voices.alloy;

        [Required]
        [MaxLength(4096)]
        public string input { get; set; } = "";
        
        [Required]
        [JsonConverter(typeof(StringEnumConverter))]
        public Formats response_Format { get; set; } = Formats.mp3;
        
        [Range(0.25, 4.0, ErrorMessage = "Value for {0} must be between {1} and {2}.")]
        public double speed { get; set; } = 1;

        public enum Voices
        {
            alloy,
            echo,
            fable,
            onyx,
            shimmer,
            nova
        }

        public enum Formats
        {
            mp3,
            opus,
            aac,
            flac
        }
    }
}