using System.Net.Http.Headers;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Polly;

namespace OpenIAIntegration.Core.Infra.OpenIA
{
    public class OpenIAClient
    {
        private readonly HttpClient _client;
        private readonly IConfiguration _configuration;
        private readonly ILogger _logger;

        public OpenIAClient(HttpClient client, 
                            ILogger<OpenIAClient> logger,
                            IConfiguration configuration)
        {
            _client = client;
            _logger = logger;
            _configuration = configuration;
            Configure();
        }

        /// <summary>
        /// Configure httpclient
        /// </summary>
        private void Configure()
        {
            _logger.LogTrace("Starting OpenIAClient config...");
            
            //lets validate some required params.
            if (_configuration.GetSection("OpenIA:ApiKey") == null)
                throw new Exception("You need to configure OpenIA Api Key at appsettings.json");

            var apiKey = _configuration.GetSection("OpenIA:ApiKey").Value.ToString();
            _client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", apiKey);
        }

        /// <summary>
        /// Execute requests async to Open IA Api.
        /// </summary>
        public async Task<T> ExecuteAsync<T>(string url, object request)
        {
            //lets create a relisient httpclient 
            //some policy to retry 3 times with an exponencial time
            var retryPolicy =  Policy
                                    .Handle<HttpRequestException>()
                                    .OrResult<HttpResponseMessage>(r => !r.IsSuccessStatusCode)
                                    .WaitAndRetryAsync(3, retryAttempt => TimeSpan.FromSeconds(Math.Pow(2, retryAttempt)));
            try
            {
                var requestContent = GetRequestContent(request);
                HttpResponseMessage response = await retryPolicy.ExecuteAsync(async () => await _client.PostAsync(url, requestContent));
                if (response.IsSuccessStatusCode)
                {
                    if (response.Content.Headers.ContentType.MediaType.ToLower().Contains("json"))
                    {
                        var responseString = await response.Content.ReadAsStringAsync();
                        return JsonConvert.DeserializeObject<T>(responseString);
                    }
                    else 
                    {
                        var result = await response.Content.ReadAsByteArrayAsync();
                        return (T)Convert.ChangeType(result, typeof(T));
                    }
                }
                else 
                {
                    var responseString = await response.Content.ReadAsStringAsync();
                    throw new Exception($"The service return status error: {response.StatusCode} with body: {responseString}");
                }
            }
            catch(Exception ex)
            {
                _logger.LogError(ex, ex.Message);
                throw;
            }
        }

        private StringContent GetRequestContent(object request)
        {
            return new StringContent(JsonConvert.SerializeObject(request), Encoding.UTF8, "application/json");
        }
    }
}