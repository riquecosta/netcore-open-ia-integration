using Microsoft.Extensions.DependencyInjection;
using OpenIAIntegration.Core.Infra.OpenIA;
using OpenIAIntegration.Core.Services;

namespace OpenIAIntegration.Core
{
    public static class OpenIAIntegrationCoreModule
    {
        public static IServiceCollection AddOpenIAIntegrationCoreModule(this IServiceCollection services)
        {
            services.AddScoped<OpenIAService>();
            services.AddHttpClient<OpenIAClient>();
            return services;
        }
    }
}