﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using OpenIAIntegration.Core;
using OpenIAIntegration.Core.Services;

IConfiguration Configuration = new ConfigurationBuilder()
    .AddJsonFile("appSettings.json", optional: false, reloadOnChange: true)
    .Build();

var serviceProvider = new ServiceCollection()
  .AddSingleton<IConfiguration>(Configuration)
  .AddOpenIAIntegrationCoreModule()
  .BuildServiceProvider();

var _openIAIntegrationService = serviceProvider.GetService<OpenIAService>();

Console.WriteLine("Write some prompt: ");
Console.WriteLine("----");
String s;
int ctr = 0;
do {
    ctr++;
    s = Console.ReadLine();
    var response = _openIAIntegrationService.Completion(s).GetAwaiter().GetResult();
    Console.WriteLine($"response: {response.choices.FirstOrDefault().message.content}");
} while (s != null);
Console.WriteLine("---");